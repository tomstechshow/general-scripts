$serverlist = 'toolbox16'
#Functions
function myTimer {
    $s = 0
    for($i=1; $i -lt $args[0]; $i++) { 
        Start-Sleep -s 1
        $s++
        if ($s -ge 10) {
            Write-Host -NoNewline '.'
            $s=0
        }
    }
}

#Main Loop
Foreach ($server in $serverlist) {
    Write-Host $Server
    Start-Process -FilePath wu-launch.bat -ArgumentList $server
    myTimer 100
    $statFile = '\\' + $server + '\c$\Scripts\status.txt'
    while ($true) {
        $status='Running'
        $data = ''
        $data = Get-Content $statFile
        foreach ($item in $data) {
            if ($item.ToString() -eq 'DONE') {
                $status='DONE'
            }
        }
        if ($status -eq 'DONE') { break }
        Write-Host - -NoNewline 'Not Done'
        myTimer 100
    }
    $data = ''
    $data = Get-Content $statFile
    $fulldate = Get-Date
    $today = $fulldate.Year.ToString() + '-' + $fulldate.Month.ToString() + '-' + $fulldate.Day.ToString()
    Write-Host $today
    $status = 'Done'
    foreach ($item in $data) {
        if($item.StartsWith('4')) {      
            Write-Host $item
        }
    }
}