;
; ----------------------------------------------------------------------------
; Ping Display Script
; ----------------------------------------------------------------------------

#include <GUIConstantsEx.au3>
#include <Array.au3>
#include <Color.au3>
#include <StaticConstants.au3>
#include <File.au3>

_Main()
Exit

Func _Main()
	; Initialize variables
	Local $iGUIWidth = 300, $iGUIHeight = 700
	Local $idEdit_1, $idButton_Ok, $idButton_Cancel, $iMsg
	local $iPlist=""
	local $labelPos = 30
	local $idLabel = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
	local $q = 1000
	
	;Read ping list file into an array
	_FileReadToArray("pinglist.txt", $iPlist, Default, ",")
	
	; Create window
	GUICreate("Ping Monitor", $iGUIWidth, $iGUIHeight, @DesktopWidth-305, 120)

	; Create labels to update with data
	For $i = 1 To $iPlist[0][0]
		$idLabel[$i] =  GUICtrlCreateLabel("Starting ....", 10, $labelPos, 280, 20)
		$labelPos = $labelPos + 30
	Next

	; Create an "OK" button
	$idButton_Ok = GUICtrlCreateButton("Stop", 115, 650, 70, 30)

	; Show window/Make the window visible
	GUISetState(@SW_SHOW)

	; Loop until:
	; - user presses Esc
	; - user presses Alt+F4
	; - user clicks the close button
	While 1
		; Update data in labels 
		$q = $q + 1
		; Check ping every 1000 loops,  if you want to check faster lower this number
		if $q>1000 Then
			For $i = 1 To $iPlist[0][0]
				Local $iPing = Ping($iPlist[$i][1], 250)
				If $iPing > 0 Then ; If a value greater than 0 was returned then display the following message.
					$newLabelText = $iPlist[$i][0] & " " & $iPing
					GUICtrlSetData($idLabel[$i],$newLabelText)
					GUICtrlSetBkColor($idLabel[$i], 0x00FF00)
				Else
					$newLabelText = $iplist[$i][0] & " " & @error
					GUICtrlSetData($idLabel[$i],$newLabelText)
					GUICtrlSetBkColor($idLabel[$i], 0xFF8888)
				EndIf
			Next
			$q=0
		EndIf
		
		; After every loop check if the user clicked something in the GUI window
		$iMsg = GUIGetMsg()
		Select
			; Check if user clicked on the close button
			Case $iMsg = $GUI_EVENT_CLOSE
				; Destroy the GUI including the controls
				GUIDelete()
				; Exit the script
				Exit
				; Check if user clicked on the "OK" button
			Case $iMsg = $idButton_Ok
				Exit
		EndSelect
	WEnd
EndFunc   ;==>_Main
