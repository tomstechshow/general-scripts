/****************************************
Example Sound Level Sketch for the 
Adafruit Microphone Amplifier
****************************************/

#include "FastLED.h"

// How many leds in your strip?
#define NUM_LEDS 50

// For led chips like Neopixels, which have a data line, ground, and power, you just
// need to define DATA_PIN.  For led chipsets that are SPI based (four wires - data, clock,
// ground, and power), like the LPD8806 define both DATA_PIN and CLOCK_PIN
#define DATA_PIN 6

// Define the array of leds
CRGB leds[NUM_LEDS];

const int sampleWindow = 50; // Sample window width in mS (50 mS = 20Hz)
unsigned int sample;
double volthigh;
int idle;

void setup() 
{
   FastLED.addLeds<WS2811, DATA_PIN, RGB>(leds, NUM_LEDS);
   Serial.begin(9600);
   volthigh = 0;
}


void loop() 
{
   unsigned long startMillis= millis();  // Start of sample window
   unsigned int peakToPeak = 0;   // peak-to-peak level

   unsigned int signalMax = 0;
   unsigned int signalMin = 1024;

   // collect data for 50 mS
   while (millis() - startMillis < sampleWindow)
   {
      sample = analogRead(0);
      if (sample < 1024)  // toss out spurious readings
      {
         if (sample > signalMax)
         {
            signalMax = sample;  // save just the max levels
         }
         else if (sample < signalMin)
         {
            signalMin = sample;  // save just the min levels
         }
      }
   }
   peakToPeak = signalMax - signalMin;  // max - min = peak-peak amplitude
   double volts = (peakToPeak * 3.5) / 1024;  // convert to volts

   int data = (peakToPeak / 6.5);

   for (int i = 0; i < 50; i++) {
    leds[i] = CRGB::Black;
   }
    int g1 = 0;
    int g2 = 19;
    int g3 = 20;
    int g4 = 39;
    int g5 = 40;
    int low = 255;
    int high = 0;
    int blu = 0;

    int incr = 10;
    for (int q=0; q<10;q++) {
    if (data >=incr) {
         if (random(25) == 10) {
             leds[g1].setRGB(random(255),random(255),random(255));
         } else {
             leds[g1].setRGB( high, low, blu);
         }
     }
     if (data >=incr) {
          if (random(25) == 10) {
             leds[g2].setRGB(random(255),random(255),random(255));
          } else {
             leds[g2].setRGB( high, low, blu);
          }
     }
     if (data >=incr) {
          if (random(25) == 10) {
              leds[g3].setRGB(random(255),random(255),random(255));
          } else {
              leds[g3].setRGB( high, low, blu);
          }
      }
      if (data >=incr) {
           if (random(25) == 10) {
               leds[g4].setRGB(random(255),random(255),random(255));
           } else { 
               leds[g4].setRGB( high, low, blu);
           }
      }
      if (data >=incr) {
           if (random(25) == 10) {
               leds[g5].setRGB(random(255),random(255),random(255));
           } else {
               leds[g5].setRGB( high, low, blu);
           }
      }

          g1++;
          g2--;
          g3++;
          g4--;
          g5++;
          incr = incr + 10;
          low = 255;
          high = 0; 
          if (incr > 70) {
            low = 255;
            high = 255;    
          }
          if (incr > 80) {
            low = 0;
            high = 255;    
          }
      }
      if (data <=6 ) {
        idle = idle + 1;
        if (idle >=30) {
          for (int i = 0; i < 25; i++) {
            leds[random(50)].setRGB(random(100),random(100),random(100)); 
          }
          delay (1000);
        }
      } else {
        idle = 0;
      }
   FastLED.show();
   if (sample > volthigh){
    volthigh = sample;
   }
  
   Serial.println(data);

}