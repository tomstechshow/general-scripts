Import-Module PSWindowsUpdate
Remove-Item -Path C:\Scripts\status.txt
Start-Transcript -Path C:\Scripts\status.txt
$Updates = "Critical Updates","Security Updates"
Get-WindowsUpdate -AcceptAll -Verbose -Install -IgnoreReboot -Category $Updates
Write-Host "DONE"
Stop-Transcript
Start-Sleep -s 200
Restart-Computer