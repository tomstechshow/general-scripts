#include <Constants.au3>
#include <GDIPlus.au3>
#include <ButtonConstants.au3>
#include <GUIConstantsEx.au3>
#include <Date.au3>
_GDIPlus_Startup()

_Main()

Func _Main()
	Local $idSeparator1, $idDate, $aUser, $sFQDN_Group, $sFQDN_User, $iResult
	Local $idCancelButton, $idUserName, $idCMDButton
	Local $iMsg, $hGraphic, $hImage, $idMainWindow
	#forceref $idSeparator1

	$idMainWindow = GUICreate("User Menu", 500, 300)

	$idChromeButton = GUICtrlCreateButton("Google Chrome", 20, 100, 120, 40)

	$idQBButton = GUICtrlCreateButton("QuickBooks", 20, 150, 120, 40)

	$idPrintButton = GUICtrlCreateButton("Printers", 150, 100, 120, 40)

	$idCMDButton = GUICtrlCreateButton("Command", 150, 150, 120, 40)

	$idDate = GUICtrlCreateLabel( _Now(), 320, 260)
	$idUserName = GUICtrlCreateLabel(@UserName, 320, 220)

	$idButtonDesktop = GUICtrlCreateButton("desktop", 0, 0, 40, 40, $BS_ICON)
	GUICtrlSetImage(-1, "shell32.dll", 35)
	$idButtonclose = GUICtrlCreateButton("close", 460, 0, 40, 40, $BS_ICON)
	GUICtrlSetImage(-1, "shell32.dll", 28)

	GUISetState()

	$hImage = _GDIPlus_ImageLoadFromFile(@ScriptDir & "\i\tts-icon.jpg")
	$hGraphic = _GDIPlus_GraphicsCreateFromHWND($idMainWindow)
	_GDIPlus_GraphicsDrawImage($hGraphic, $hImage, 320 , 60)

	While True
		Switch GUIGetMsg()
			Case $GUI_EVENT_CLOSE 
				ExitLoop
			Case $idButtonclose
				ExitLoop
			Case $idQBButton
				Run("C:\Program Files (x86)\Intuit\QuickBooks 2017\QBW32.exe")
			Case $idPrintButton
				ShellExecute("c:\Users\Public\Printers.lnk")
			Case $idChromeButton
				Run("C:\Program Files (x86)\Google\Chrome\Application\chrome.exe")
			Case $idButtonDesktop
				Run("explorer.exe")
			Case $idCMDButton
				Run("cmd.exe")
			Case Else
				_GDIPlus_GraphicsDrawImage($hGraphic, $hImage, 320 , 60)
		EndSwitch
		GUICtrlSetData($idDate,_Now())
	WEnd

	GUIDelete()

	Shutdown(0)

	Exit
EndFunc   ;==>_Main
