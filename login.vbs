'**********************************************
'        Corporate Logon Script
'        Created By: Tom Nelson
'           Version: 1.4.1 Dev
'        Created On: June 11, 2008
'
'     11/17/08 - Added section for RMC Test server Clients
'     08/13/08 - Added section to updated microsoft excel with a hotfix.
'     06/24/08 - Added section to remove login batch file from profile
'     06/23/08 - V.1.2 Adding existing Groups for drive mapping
'     06/17/08 - Deployed to AD for all users.
'     06/13/08 - Added create icon for RMC Clients.
'     03/22/10 - Updated RMC With New Create Icon Code
'     04/06/10 - Added RDP Icon mapping.
'     04/14/10 - Cleaned up old code sections
'
'     For debugging put a single quote
'     in front of the next line:
     On Error Resume Next
'
'**********************************************


'*** Start Connect Class Libraries
    Set objDictionary = CreateObject("Scripting.Dictionary")
    Set objNetwork = CreateObject("Wscript.Network")
    Set objADSysInfo = CreateObject("ADSystemInfo")
    Set WshShell = WScript.CreateObject("WScript.Shell")
    Set WshProcessEnv = WshShell.Environment("Process")
'*** End

'*** Start Get User and Computer Information
    strUser = objADSysInfo.UserName
    strComputer = objADSysInfo.ComputerName
    Set objUser = GetObject("LDAP://" & strUser)
    set objComputer = GetObject("LDAP://" & strComputer)
    arrOUs = Split(objComputer.Parent, ",")
    arrMainOU = Split(arrOUs(0), "=")
    ComputerOU = arrMainOU(1)
    strLoginName = objUser.sAMAccountName
'*** End

'*** Start Flag Computer Description With Current User
    strMessage = objUser.CN & " (" & objUser.displayname & ") logged on at " & Now & "."
    objComputer.Put "description", strMessage
    objComputer.SetInfo
'*** End

'*** Start Get List of Groups User Belongs To
    AChk = isarray(objUser.MemberOf)
    If AChk = 0 Then
        strGroupPath = "LDAP://" & objUser.MemberOf
        Set objGroup = GetObject(strGroupPath)
        strGroupName = objGroup.CN
        objDictionary.Add strGroupName, strGroupName
    Elseif AChk = -1 Then
            For Each strGroup in objUser.MemberOf
            Set objGroup = GetObject("LDAP://" & strGroup)
            strGroupName = objGroup.CN
            objDictionary.Add strGroupName, strGroupName  
        Next
    End If
'*** End

Set fs = CreateObject("Scripting.FileSystemObject")

'*** Start Map Network Drives
     If objDictionary.Exists("F9Users") Then
        objNetwork.RemoveNetworkDrive "N:"
        objNetwork.MapNetworkDrive "N:", "\\ERPF9\F9Data"
     End If
     If objDictionary.Exists("F9Users") Then
        objNetwork.RemoveNetworkDrive "M:"
        objNetwork.MapNetworkDrive "M:", "\\CUSTOMER\MacApps"
     End If
     If objDictionary.Exists("Ceridian Users") Then
        objNetwork.RemoveNetworkDrive "R:"
        objNetwork.MapNetworkDrive "R:", "\\payday\Ceridian"
     End If

     If objDictionary.Exists("RMCClients") Then
        DesktopPath = WSHShell.SpecialFolders("Desktop")
        objNetwork.RemoveNetworkDrive "T:"
        objNetwork.MapNetworkDrive "T:", "\\ERPBar1\RMCShare"

        Set theShortcut = WSHShell.CreateShortcut(DesktopPath & "\RMC-101.lnk")
        Notes1 = objUser.get("info")
        StartPos=InStr(1,Notes1,"#RMC#")+5
        EndPos=InStr(StartPos,Notes1,"#RMC#")
        EndString=EndPos-StartPos
        CmdLine=Mid(Notes1,StartPos,EndString)
        theShortcut.TargetPath = WSHShell.ExpandEnvironmentStrings("T:\bclient\bclient.exe")
        theShortcut.Arguments =  " " & CmdLine
        theShortcut.WorkingDirectory = WSHShell.ExpandEnvironmentStrings("T:\bclient")
        theShortcut.WindowStyle = 4
        theShortcut.IconLocation = WSHShell.ExpandEnvironmentStrings("T:\bclient\bclient.exe, 0")
        theShortcut.Save
    End If

'*** Webtop Clear Files
    Sub recurse(ByRef objFolders)
        Set objSubFolders = objFolders.SubFolders
        Set objFiles = objFolders.Files
        for each File in objFiles
            File.Delete
        Next
        for each Folder in objSubFolders
            Recurse Folder
        Next
        Set objSubFolders = Nothing
        Set objFiles = Nothing
    end Sub

    strFolder = "C:\Documents and Settings\" & strLoginName & "\Application Data\Sun\Java\Deployment\cache\"
    Set objFSO     = CreateObject("Scripting.FileSystemObject")
    Set objFolders     = objFSO.GetFolder(strFolder)
    Recurse objFolders

    strFolder = "C:\Documents and Settings\" & strLoginName & "\WebtopMenuCache\"
    Set objFSO     = CreateObject("Scripting.FileSystemObject")
    Set objFolders     = objFSO.GetFolder(strFolder)
    Recurse objFolders


    If (ComputerOU = "Foothills") or (ComputerOU = "Foothills-Executive") Then
        Const ForReading = 1
        Const ForWriting = 2
        Set objFSo = CreateObject("Scripting.FileSystemObject")
        strFileName = "C:\Documents and Settings\" & strLoginName & "\Application Data\Sun\Java\Deployment\deployment.properties"
        Set objFile = objFSO.OpenTextFile(strFileName, ForReading)
        strContents = objFile.ReadAll
        objFile.Close
        arrLines = Split(strContents, vbCrLf)
        Set objFile = objFSO.OpenTextFile(strFileName, ForWriting)
        For i = 0 to UBound(arrLines)
            if arrLines(i) = "deployment.cache.enabled=false" then
                objFile.WriteLine "deployment.cache.enabled=true"
            else
                objFile.WriteLine arrLines(i)
            end if
        Next
        objFile.Close
    End If
'*** Webtop Clear END


    If objDictionary.Exists("RDPUsers") Then
        DesktopPath = WSHShell.SpecialFolders("Desktop")
        strFilePath = "\\dns1\SYSVOL\prideindustries.com\push\ConnectToSystem.rdp"
        strDestination = DesktopPath & "\ConnectToSystem.rdp"
        Set objFileCopy = objFSO.GetFile(strFilePath)
        objFileCopy.Copy (strDestination)
    End If

    objNetwork.RemoveNetworkDrive "P:"
    objNetwork.MapNetworkDrive "P:", "\\FP1\Public"

    objNetwork.RemoveNetworkDrive "U:"
    homedrive = "\\FP1\" & objUser.sAMAccountName & "$"
    objNetwork.MapNetworkDrive "U:", homedrive

'
'*** End